FROM alpine

LABEL maintainer="mlreef.com"

ARG CI_COMMIT_REF_SLUG
ARG BUILD_DATE

# Metadata
LABEL org.label-schema.ci-commit-ref-slug=$CI_COMMIT_REF_SLUG \
      org.label-schema.commit-url="https://gitlab.com/mlreef/devops/k8s-images" \
      org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.docker.dockerfile="/Dockerfile"

ENV KUBE_LATEST_VERSION="v1.20.0"

RUN apk add --update ca-certificates \
    curl                             \
    bash                             \
    jq                               \
 && apk add --update -t deps \
 && export ARCH="$(uname -m)" && if [[ ${ARCH} == "x86_64" ]]; then export ARCH="amd64"; fi && curl -L https://storage.googleapis.com/kubernetes-release/release/${KUBE_LATEST_VERSION}/bin/linux/${ARCH}/kubectl -o /usr/local/bin/kubectl \
 && chmod +x /usr/local/bin/kubectl \
 && apk del --purge deps \
 && rm /var/cache/apk/*

WORKDIR /root
ENTRYPOINT ["kubectl"]
CMD ["help"]
